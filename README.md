﻿# Bring/Posten zipcode CLR for SQL Server

## How to enable in SQL server
```
sp_configure 'show advanced options', 1;
GO
RECONFIGURE;
GO
sp_configure 'clr enabled', 1;
GO
RECONFIGURE;
GO
```

```
ALTER DATABASE [DB] SET TRUSTWORTHY ON
```

```
CREATE ASSEMBLY Posten 
from 'X:\SqlServerBringPostnummer\SqlServerBringPostnummer.dll' 
WITH PERMISSION_SET = UNSAFE
```

```
CREATE PROCEDURE PostenOppslag @postnummer nvarchar(4)
AS
EXTERNAL NAME Posten.Posten.Oppslag
```

## Execute SP
```
exec PostenOppslag '0000' -- "Ugyldig postnummer", 0
exec PostenOppslag '1778' -- "HALDEN", 1
exec PostenOppslag '6518' -- "KRISTIANSUND N", 1
```