﻿using System;
using System.Data;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Posten
{
   
    [SqlProcedure]
    public static void Oppslag(string postnummer)
    {
        var obj = GetPostenResult(postnummer);
        var record = CreateSqlDataRecord();
        using (var scope = SqlResultsScope.Start(record))
        {
            record.SetString(0, obj["result"].Value<string>());
            record.SetBoolean(1, obj["valid"].Value<bool>());
            scope.Send(record);
        }
    }

    private static SqlDataRecord CreateSqlDataRecord()
    {
        return new SqlDataRecord(
            new SqlMetaData("Place", SqlDbType.NVarChar, 4000),
            new SqlMetaData("Valid", SqlDbType.Bit)
            );
    }

    private static JObject GetPostenResult(string postnummer)
    {
        var url =
            $"https://api.bring.com/shippingguide/api/postalCode.json?clientUrl=on-it.no&country=no&pnr={postnummer}";
        var data = Task.Run(async () => await GetResultFromPosten(url)).Result;
        var obj = JsonConvert.DeserializeObject<JObject>(data);
        return obj;
    }

    private static async Task<string> GetResultFromPosten(string url)
    {
        using (var client = new HttpClient())
        {
            var response = await client.GetAsync(url);
            return await response.Content.ReadAsStringAsync();
        }
    }
    private class SqlResultsScope : IDisposable
    {
        private SqlResultsScope(SqlDataRecord record)
        {
            SqlContext.Pipe.SendResultsStart(record);
        }

        public void Send(SqlDataRecord record)
        {
            SqlContext.Pipe.SendResultsRow(record);
        }

        public void Dispose()
        {
            SqlContext.Pipe.SendResultsEnd();
        }

        public static SqlResultsScope Start(SqlDataRecord record)
        {
            return new SqlResultsScope(record);
        }
    }
}